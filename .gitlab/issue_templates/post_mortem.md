## TLDR

(Quick summary of what happened / the issue)

### The underlying issues

(What was the underlying issue? - include screenshots were possible)

### How was it resolved

(What steps were taken to resolve it?)

## Timeline

(Timeline from initial reports and diagnosis through to the solution - include screenshots were possible)

## Impacted metrics

(Labels for metrics that were impacted)

## How can triage be improved in the future?

(Future steps to prevent this from happening again, or improve alerting)
