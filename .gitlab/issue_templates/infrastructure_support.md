<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "Type::Infrastructure Support" label and verify the issue you're about to submit isn't a duplicate.
--->

### Summary

(Summarize the infrastructure issue encountered concisely)

***NOTE***: This template is for **NON-SUPERHERO** support requests. If you believe your issue is a Superhero, follow [these](https://developers.minds.com/docs/handbook/superhero#how-to-declare-a-superhero) steps to confirm and declare an incident.

### Steps to reproduce

(How one can reproduce the issue - this is very important)

### Environment

- [ ] Sandbox
- [ ] Staging
- [ ] Canary
- [ ] Production

### Platform information

(Browser, device, system stats)

### What is the current *bug* behavior?

(What actually happens)

### What is the expected *correct* behavior?

(What you should see instead)

### Relevant logs and/or screenshots

(Paste any relevant logs - please use code blocks (```) to format console output,
logs, and code as it's very hard to read otherwise.)

### Relevant monitoring information

(If present, link to any Sentry alarms or Grafana dashboards that may be relevant)

### Possible fixes

(If you can, mention the service or infrastructure component that might be responsible for the problem)

/label ~"Type::Infrastructure Support" ~"Priority::1 - High"
